/*
    File: "FFT.h"
    Author: Hans-Edward Hoene

    This file declares the Fast Fourier Transform functions.
*/

#ifndef _FFT_H_
#define _FFT_H_

#include "cplx.h"

/*
    Purpose: Run a 1-D DFT
    Arguments:
        [in,out] cplx          data[] - data
        [in    ] unsigned int  size - array size
        [in    ] int           forward - boolean: forward DFT?  Otherwise,
                                         inverse (reverse) DFT.
    Return:
        True on success, false on failure.  A radix-2 FFT is used, so failure
        occurs when the size is not a power of 2.
*/
int DFT(cplx data[], unsigned int size, int forward);

#endif
