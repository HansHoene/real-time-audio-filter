/*
    File: "MemMgr.h"
    Author: Hans-Edward Hoene
    
    This module defines a buffer to be passed around.  In addition, this module
    defines functions for allocating and freeing buffers.
*/

#ifndef _MEMMGR_H_
#define _MEMMGR_H_

#include "Audio.h"  // sample

// define size of buffer
#ifndef BfrSize
#define BfrSize (32768)
#endif

/* buffer */
typedef struct {
    sample data[BfrSize];
} Buffer;

/*
    Purpose: Initialise memory manager
    Return:
        TRUE on success, FALSE on failure
*/
void InitMemMgr();

/*
    Purpose: Allocate a buffer of length BfrSize to store samples
    Return:
        Pointer to buffer
*/
Buffer *AllocateBuffer();

/*
    Purpose: Free a buffer
    Arguments:
        sample *bfr - pointer to buffer
*/
void FreeBuffer(Buffer *bfr);

/*
    Purpose: Destroy Memory Manager
*/
void DestroyMemMgr();

#endif
