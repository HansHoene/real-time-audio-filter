/*
    File: "FilterAudio.c"
    Author: Hans-Edward Hoene
    
    This main function filters an audio file in small chunks.
    
    This function takes three command line arguments.
    (1) Input Audio
    (2) Output Audio
    (3) Buffer Size
*/

#include "Audio.h"
#include "FFT.h"
#include "cplx.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

#define NumArgs     (3)
#define ArgIn       (1)
#define ArgOut      (2)
#define ArgBfrSize  (3)

#define FALSE       (0)
#define TRUE        (!FALSE)

// filter bandwidth
#ifndef D0
#define D0 (400)
#endif

// filter order
#ifndef Order
#define Order (1)
#endif

// FilterType = 0 for low-pass filter or 1 for high-pass filter
#ifndef FilterType
#define FilterType (0)
#endif

#define DIFF(a, b)  (((a) > (b)) ? ((a) - (b)): ((b) - (a)))

// filter audio stream one buffer at a time
void FilterAudioStream(FILE *in, FILE *out,
                       short int *bfr, unsigned int bfrSize);

// convert samples to complex values
void SamplesToCplx(short int samples[], cplx complex[], unsigned int size);

// convert complex values into samples
void CplxToSamples(cplx complex[], short int samples[], unsigned int size);

// apply a filter
void ApplyFilter(cplx data[], unsigned int size);

int main(int argc, char *argv[]) {
    FILE            *in;        // input audio
    FILE            *out;       // output audio
    
    short int       *bfr;       // buffer
    unsigned int    bfrSize;    // buffer size
    
    int             flag;       // error flag
    
    // verify # of arguments
    if (argc != (NumArgs + 1)) {
        printf("\a*** Error: invalid number of arguments\n");
        printf("Argument List:\n");
        printf("  1) Input Audio File\n");
        printf("  2) Output Audio File\n");
        printf("  3) Buffer Size\n");
        return 0;
    }
    
    // read buffer size
    flag = 1 - sscanf(argv[ArgBfrSize], "%u", &bfrSize);
    if (flag) {
        printf("\a*** Error: could not read buffer size\n");
        return 0;
    }
    if ((bfrSize & (bfrSize - 1)) != 0) {
        printf("\a*** Error: buffer size must be power of 2\n");
        return 0;
    }
    printf("Buffer Size = %u\n", bfrSize);
    printf("Frequencies can be detected from %lf to %lf\n",
           -44100 / 2.0, 44100 / 2.0);
    printf("Frequency Resolution = %lf frequencies per buffer value\n",
           44100.0 / bfrSize);
    printf("Milliseconds per buffer = %lf\n\n", bfrSize * 1000.0 / 44100.0);
    
    // open input audio file
    in = OpenAudioInput(argv[ArgIn]);
    if (in == NULL) {
        printf("\a*** Error: could not open input audio stream\n");
        return 0;
    }
    
    // allocate buffer
    bfr = (short int *)malloc(sizeof(short int) * bfrSize);
    if (bfr == NULL) {
        CloseAudio(in);
        printf("\a*** Error: could not allocate buffer\n");
        return 0;
    }
    
    // open output audio file
    out = OpenAudioOutput(argv[ArgOut]);
    if (out == NULL) {
        CloseAudio(in);
        free(bfr);
        printf("\a*** Error: could not open output audio stream\n");
        return 0;
    }
    
    // filter audio stream
    FilterAudioStream(in, out, bfr, bfrSize);
    
    // clean up
    CloseAudio(in);
    CloseAudio(out);
    free(bfr);
    
    return 0;
}

void FilterAudioStream(FILE *in, FILE *out,
                       short int *bfr, unsigned int bfrSize) {
    unsigned int numRead;
    unsigned int i;
    cplx *data;
    
    // allocate complex array
    data = (cplx *)malloc(sizeof(cplx) * bfrSize);
    if (data == NULL) {
        printf("\a*** Error: could not allocate a buffer of complex "
               "floating-point values\n");
        return;
    }
    
    // read into buffer until empty
    while ((numRead = ReadAudio(in, bfr, bfrSize)) > 0) {
        // pad unfilled part of buffer with zeroes
        for (i = numRead; i < bfrSize; i++) {
            bfr[i] = 0;
        }
        
        // copy buffer contents into array of complex values
        SamplesToCplx(bfr, data, bfrSize);
        
        // convert time domain samples to frequency domain
        DFT(data, bfrSize, TRUE);
        
        // filter
        ApplyFilter(data, bfrSize);
        
        // convert frequencies back to time domain
        DFT(data, bfrSize, FALSE);
        
        // convert complex values to samples
        CplxToSamples(data, bfr, bfrSize);
        
        // write result to output stream
        WriteAudio(out, bfr, numRead);
    }
    
    free(data);
}

void SamplesToCplx(short int samples[], cplx complex[], unsigned int size) {
    unsigned int i;
    
    for (i = 0; i < size; i++) {
        complex[i].real = (float) samples[i];
        complex[i].imag = (float) 0;
    }
}

void CplxToSamples(cplx complex[], short int samples[], unsigned int size) {
    unsigned int    i;
    
    for (i = 0; i < size; i++) {
        samples[i] = (short int) roundf( sqrtf(
            (complex[i].real * complex[i].real) +
            (complex[i].imag * complex[i].imag)
        ));
        if (complex[i].real < 0) {
            samples[i] *= -1;
        }
    }
}

void ApplyFilter(cplx data[], unsigned int size) {
    unsigned int i;
    unsigned int centre;
    unsigned int dist;
    float invH;
    
    centre = (size >> 1);
    for (i = 0; i < size; i++) {
        dist = DIFF(i, centre);
        
        #if FilterType == 0
        invH = (dist * 44100.0 / size) / D0;
        #else
        invH = D0 / (dist * 44100.0 / size);
        #endif
        invH = 1 + pow(invH, Order * 2);
        
        data[i].real /= invH;
        data[i].imag /= invH;
    }
}
