/*
    File: "cplx.c"
    Author: Hans-Edward Hoene
*/

#include "cplx.h"

void MultiplyComplex(cplx *dst, cplx *a, cplx *b) {
    cplx temp;
    
    temp.real = (a->real * b->real) - (a->imag * b->imag);
    temp.imag = (a->real * b->imag) + (a->imag * b->real);
    
    *dst = temp;
}

void AddComplex(cplx *dst, cplx *a, cplx *b) {
    cplx temp;
    
    temp.real = a->real + b->real;
    temp.imag = a->imag + b->imag;
    
    *dst = temp;
}

void SubtractComplex(cplx *dst, cplx *a, cplx *b) {
    cplx temp;
    
    temp.real = a->real - b->real;
    temp.imag = a->imag - b->imag;
    
    *dst = temp;
}
