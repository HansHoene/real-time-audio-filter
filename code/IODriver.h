/*
    File: "IODriver.h"
    Author: Hans-Edward Hoene
    
    This file declares functions for operating the IO drivers.  Once
    initialised, this module will start two IO threads.  The input thread will
    fill a buffer with microphone samples and pass it to the processor module.
    The output thread will obtain a buffer of samples and write it to the
    speakers.
*/

#ifndef _IODRIVER_H_
#define _IODRIVER_H_

#include "Audio.h"  // sample
#include "MemMgr.h"

/*
    Purpose: Initialise the IO module.  Start the two threads.
*/
void InitIO();

/*
    Purpose: Get the next buffer from the input thread
    Return:
        Pointer to sample buffer
*/
Buffer *GetBuffer();

/*
    Purpose: Put a buffer for the output thread
    Arguments:
        [in ] Buffer *buffer - pointer to sample buffer
*/
void PutBuffer(Buffer *bfr);

/*
    Purpose: Kill the IO module.  Stop the two threads.
*/
void KillIO();

#endif
