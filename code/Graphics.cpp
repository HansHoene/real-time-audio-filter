/*
    File: "Graphics.cpp"
    Author: Hans-Edward Hoene
    
    In this source code file, a thread is started.  The thread is responsible
    for graphics, which are passed over to the display buffer.
*/

#include "Graphics.hpp"

#include "cplx.h"
#include "Audio.h"
#include "assert.h"
#include "MemMgr.h"
#include "IODriver.h"
#include "Processor.h"

#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <windows.h>
#include <GL/glut.h>
#include <math.h>
#include <errno.h>
//#include <openglut.h>

#define xCoor(x) ((2 * (x) / (res * (4.0 + (xspace * 5)))) - 1)
#define yCoor(x) ((2 * (x) / (res * (1.0 + (yspace * 2)))) - 1)

/* ----------------------------- G l o b a l s ------------------------------ */

#define FreqRange       (12000)
// 2.0 * FreqRange * BfrSize / Fs but cut off left half, so divide by 2
#define ArrSize         (8916)
#define Arr2Size        (500)

#define res             (1.0)
#define xspace          (0.2)
#define yspace          (0.2)

#define ScreenWidth     (1000)
#define ScreenHeight    (250)
#define PositionX       (10)
#define PositionY       (10)

#define DataMin         (-3000)
#define DataMax         (3000)
#define SpecMin         (0)
#define SpecMax         (100.0)

#define AxisWidth       (5)
#define LineWidth       (3)

static DisplayBfr   dispBfr;
static DisplayBfr   display;
static sem_t        bfrEmpty;
static sem_t        bfrFull;

static int qw = 0;
static int draw = 0;

/* -------------------- H e l p e r    F u n c t i o n s -------------------- */
void displayGraphics(void);
void ProcessKeyboard(unsigned char key, int x,int y);
void idle(void);

void GraphicsMain();

/* ----------------------- F u n c t i o n    D e f s ----------------------- */

int main() {
    //for (unsigned int i = 0; i < 4; i++) {printf("AXIS %u\n", i);
    //printf("  (%f, %f) to (%f, %f)", res * ((xspace * (i + 1)) + (1.0 * i)), res * yspace, res * ((xspace * (i + 1)) + (1.0 * (i + 1))), res * yspace);
    //printf("  (%f, %f) to (%f, %f)", res * ((xspace * (i + 1)) + (1.0 * i)), res * yspace, res * ((xspace * (i + 1)) + (1.0 * i)), res * (yspace + 1.0));}if(1){return 0;}
    
    InitGraphics();
    InitMemMgr();
    InitIO();
    StartProcessor();
    
    GraphicsMain();
    
    // unreachable
    
    return 0;
}

void InitGraphics() {
    int flag;
    
    flag = sem_init(&bfrEmpty, 0, 1);
    Assert(flag == 0, "Could not create semaphore");
    
    flag = sem_init(&bfrFull, 0, 0);
    Assert(flag == 0, "Could not create semaphore");
}

static int counter = 0;
void UpdateGraphics(const DisplayBfr *bfr) {
    unsigned int centre;
    int flag;
    const int rollovervalue = 1;
    
    centre = BfrSize >> 1;
    
    ++counter;
    if (counter < rollovervalue) {
        return;
    } else {
        counter = 0;
    }
    
    // try to wait for empty graphics buffer
    flag = sem_trywait(&bfrEmpty);
    if (flag == EAGAIN) {
        // the graphics module is not ready, so give up
        return;
    }
    Assert(flag == 0, "Semaphore error");
    
    /* copy data to graphics buffer */
    
    // copy input data
    memcpy(dispBfr.dataIn, &bfr->dataIn[centre], sizeof(float) * Arr2Size);
    
    // copy input spectrum
    memcpy(dispBfr.specIn, &bfr->specIn[centre], sizeof(float) * ArrSize);
    //for (flag = 0; flag < ArrSize; ++flag) printf("%f, ", dispBfr.specIn[flag]);
    
    // copy output spectrum
    memcpy(dispBfr.specOut, &bfr->specOut[centre], sizeof(float) * ArrSize);
    
    // copy output data
    memcpy(dispBfr.dataOut, &bfr->dataOut[centre], sizeof(float) * Arr2Size);
    
    // indicate full graphics buffer
    qw++;
    sem_post(&bfrFull);
}

void DestroyGraphics() {
    sem_destroy(&bfrEmpty);
    sem_destroy(&bfrFull);
    //glutLeaveMainLoop();
    //glutDestroyWindow();
}

void GraphicsMain() {
    int argc = 1;
    char *argv[1] = {(char *)"not_used"};
    
    // initialise
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(ScreenWidth, ScreenHeight);
    glutInitWindowPosition(PositionX, PositionY);
    glutCreateWindow("Real-Time Audio Frequency Spectrum Analyser");

    // set up handlers
    glutDisplayFunc(displayGraphics);
    glutKeyboardFunc(ProcessKeyboard);
    glutIdleFunc(idle);
    
    // initialise
    // white coloured background
    glClearColor(1.0,1.0,1.0,1.0);
    //glViewport(0, 0, ScreenWidth, ScreenHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-1, 1, -1, 1);
    printf("%lf, %lf\n", res * (4.0 + (xspace * 5)), res * (1.0 + (yspace * 2)));
    
    // start
    glutMainLoop();
}

void displayGraphics(void) {
    float x, y;
    float offX, offY;
    unsigned int i;
    
    const char *str[4] = {
        "Input Data", "Input Spectrum",
        "Output Spectrum", "Output Data"
    };
    char *c;
    
    if (draw <= 0) {
        return;
    } else {
        --draw;
    }
    printf("Graphics Update %d\n", draw);
    
    // clear canvas
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    
    // draw functions
    glLineWidth(LineWidth);
    glColor3f(1.0, 0.0, 0.0);
    
    // define data in
    offX = res * xspace;
    offY = res * yspace;
    glBegin(GL_LINE_STRIP);
    for (i = 0; i < Arr2Size; i++) {
        x = offX + ((res * i) / Arr2Size);
        y = offY + display.dataIn[i];
        glVertex2f(xCoor(x), yCoor(y));
    }
    glEnd();
    
    // define input spectrum
    offX = res * ((xspace * 2) + 1);
    offY = res * yspace;
    glBegin(GL_LINE_STRIP);
    for (i = 0; i < ArrSize; i++) {
        x = offX + ((res * i) / ArrSize);
        y = offY + display.specIn[i];
        glVertex2f(xCoor(x), yCoor(y));
    }
    glEnd();
    
    // define output spectrum
    offX = res * ((xspace * 3) + 2);
    offY = res * yspace;
    glBegin(GL_LINE_STRIP);
    for (i = 0; i < ArrSize; i++) {
        x = offX + ((res * i) / ArrSize);
        y = offY + display.specOut[i];
        glVertex2f(xCoor(x), yCoor(y));
    }
    glEnd();
    
    // define output data
    offX = res * ((xspace * 4) + 3);
    offY = res * yspace;
    glBegin(GL_LINE_STRIP);
    for (i = 0; i < Arr2Size; i++) {
        x = offX + ((res * i) / Arr2Size);
        y = offY + display.dataOut[i];
        glVertex2f(xCoor(x), yCoor(y));
    }
    glEnd();
    
    // draw axis for each of the four graphs
    glLineWidth(AxisWidth);
    glColor3f(0.0, 0.0, 0.0);
    for (i = 0; i < 4; i++) {
        //printf("AXIS %u\n", i);
        // x-axis
        glBegin(GL_LINE_STRIP);
        glVertex2f(
            xCoor(res * ((xspace * (i + 1)) + (1.0 * i))),
            yCoor(res * (yspace + (((i % 3) == 0) ? 0.5 : 0)))
        );
        glVertex2f(
            xCoor(res * ((xspace * (i + 1)) + (1.0 * (i + 1)))),
            yCoor(res * (yspace + (((i % 3) == 0) ? 0.5 : 0)))
        );
        glEnd();
        //printf("  (%f, %f) to (%f, %f)\n", res * ((xspace * (i + 1)) + (1.0 * i)), res * yspace, res * ((xspace * (i + 1)) + (1.0 * (i + 1))), res * yspace);
        
        // y-axis
        glBegin(GL_LINE_STRIP);
        glVertex2f(
            xCoor(res * ((xspace * (i + 1)) + (1.0 * i))),
            yCoor(res * yspace)
        );
        glVertex2f(
            xCoor(res * ((xspace * (i + 1)) + (1.0 * i))),
            yCoor(res * (yspace + 1.0))
        );
        glEnd();
        
        glRasterPos2f(// 2f ?
            xCoor(res * ((xspace * (i + 1)) + (1.0 * i))),
            yCoor(res * yspace / 3.0)
        );
        for (c = (char *)str[i]; *c != '\0'; c++) {
            glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
        }
    }
    
    // draw
    glFlush();
    glutSwapBuffers();
}

void ProcessKeyboard(unsigned char key, int x, int y) {
    if ((key >= 'a' && key <= 'z') || (key >= 'A' && key <= 'Z') || key == 27) {
        KillIO();
        StopProcessor();
        DestroyGraphics();
        DestroyMemMgr();
        exit(0);
    }
}

void idle(void) {
    int flag;
    unsigned int i;
    
    // try to wait for full graphics buffer
    flag = sem_trywait(&bfrFull);
    if (flag) {// == EAGAIN) {
        // the graphics module is not ready, so give up
        return;
    }
    //printf("flag = %d\n", flag);
    //printf("%s\n", strerror(flag));
    //printf("%s\n", strerror(errno));
    Assert(flag == 0, "Semaphore error");
    
#ifdef DISPLOG
    // convert spectrum to log
    for (i = 0; i < ArrSize; i++) {
        dispBfr.specIn[i] = log(dispBfr.specIn[i]);
        dispBfr.specOut[i] = log(dispBfr.specOut[i]);
    }
#endif
    
    // normalise and clamp spectral values
    for (i = 0; i < ArrSize; i++) {
        dispBfr.specIn[i] = dispBfr.specIn[i] > SpecMax ? SpecMax :
                            dispBfr.specIn[i] < SpecMin ? SpecMin :
                            dispBfr.specIn[i];
        dispBfr.specIn[i] =
            res * (dispBfr.specIn[i] - SpecMin) / (SpecMax - SpecMin);
        
        dispBfr.specOut[i] = dispBfr.specOut[i] > SpecMax ? SpecMax :
                             dispBfr.specOut[i] < SpecMin ? SpecMin :
                             dispBfr.specOut[i];
        dispBfr.specOut[i] =
            res * (dispBfr.specOut[i] - SpecMin) / (SpecMax - SpecMin);
        
        display.specIn[i] = dispBfr.specIn[i];
        display.specOut[i] = dispBfr.specOut[i];
    }
    
    // normalise and clamp data values
    for (i = 0; i < Arr2Size; i++) {
        dispBfr.dataIn[i] = dispBfr.dataIn[i] > DataMax ? DataMax :
                            dispBfr.dataIn[i] < DataMin ? DataMin :
                            dispBfr.dataIn[i];
        dispBfr.dataIn[i] =
            res * (dispBfr.dataIn[i] - DataMin) / (DataMax - DataMin);
        
        dispBfr.dataOut[i] = dispBfr.dataOut[i] > DataMax ? DataMax :
                             dispBfr.dataOut[i] < DataMin ? DataMin :
                             dispBfr.dataOut[i];
        dispBfr.dataOut[i] =
            res * (dispBfr.dataOut[i] - DataMin) / (DataMax - DataMin);
            
        display.dataIn[i] = dispBfr.dataIn[i];
        display.dataOut[i] = dispBfr.dataOut[i];
    }
    
    // draw data from graphics buffer
    glutPostRedisplay();
    
    // indicate empty graphics buffer
    ++draw;printf("REDRAW------------------\n");
    sem_post(&bfrEmpty);
}
