/*
    File: "Audio.c"
    Author: Hans-Edward Hoene
*/

#include "Audio.h"

#include <stdio.h>  // size_t, FILE, sprintf
#include <stdint.h> // sint16_t

#define StrBfrSize      (256)   // max size of temporary strings
#define BytesPerSample  (2)     // every sample is two bytes (16 bits)
#define NumChannels     (2)     // stereo audio (not mono)
#define Fs              (44100) // sample rate

FILE *OpenAudioInput(const char fname[]) {
    FILE    *pipe;              // handle to pipe
    char    cmd[StrBfrSize];    // command as a string
    
    // create command
    // this command will read samples from audio file
    sprintf(cmd, "ffmpeg -hide_banner -loglevel \"quiet\" -i \"%s\" -vn "
                 "-acodec pcm_s16le -f s16le -ar %u -ac %u -",
            fname, Fs, NumChannels);
    
    // run command; open pipe to output
    pipe = popen(cmd, "rb");
    
    return pipe;
}

size_t ReadAudio(FILE *stream, sample bfr[], unsigned int bfrSize) {
    return fread(bfr, BytesPerSample, bfrSize, stream);
}

FILE *OpenAudioOutput(const char fname[]) {
    FILE    *pipe;              // handle to pipe
    char    cmd[StrBfrSize];    // command as a string
    
    // create command
    // this command will write samples to audio file
    sprintf(cmd, "ffmpeg -hide_banner -loglevel \"quiet\" -y -f s16le "
                 "-ar %u -ac %u -acodec pcm_s16le -vn -i - \"%s\"",
            Fs, NumChannels, fname);
    
    // run command; open pipe to output
    pipe = popen(cmd, "wb");
    
    return pipe;
}

size_t WriteAudio(FILE *stream, sample bfr[], unsigned int bfrSize) {
    return fwrite(bfr, BytesPerSample, bfrSize, stream);
}

FILE *OpenMicrophone(const char device[]) {
    FILE    *pipe;              // handle to pipe
    char    cmd[StrBfrSize];    // command as a string
    
    // create command
    sprintf(cmd, "ffmpeg -hide_banner -loglevel \"quiet\" -f dshow "
                 "-i audio=\"%s\" -f s16le -acodec pcm_s16le "
                 "-vn -ar %u -ac %u -",
            device, Fs, NumChannels);
    
    // run command; open pipe to input
    pipe = popen(cmd, "rb");
    if (pipe == NULL) {
        return pipe;
    }
    
    // no buffer
    setbuf(pipe, NULL);
    return pipe;
}

FILE *OpenLinuxMicrophone() {
    FILE    *pipe;              // handle to pipe
    char    cmd[StrBfrSize];    // command as a string
    
    // create command
    sprintf(cmd, "ffmpeg -hide_banner -loglevel \"quiet\" -f alsa "
                 "-i hw:0 -f s16le -acodec pcm_s16le "
                 "-vn -ar %u -ac %u -",
            Fs, NumChannels);
    
    // run command; open pipe to input
    pipe = popen(cmd, "rb");
    if (pipe == NULL) {
        return pipe;
    }
    
    // no buffer
    setbuf(pipe, NULL);
    return pipe;
}

FILE *OpenSpeaker() {
    FILE    *pipe;              // handle to pipe
    char    cmd[StrBfrSize];    // command as a string
    
    // create command
    sprintf(cmd, "ffplay -hide_banner -loglevel \"quiet\" -nodisp -autoexit "
                 "-f s16le -vn -ar %u -ac %u -acodec pcm_s16le -",
            Fs, NumChannels);
    
    // run command; open pipe to output
    pipe = popen(cmd, "wb");
    if (pipe == NULL) {
        return pipe;
    }
    
    // no buffer
    setbuf(pipe, NULL);
    return pipe;
}

long int CountBufferedData(FILE *stream) {
    long int    totalSize;  // total stream size
    long int    curPos;     // current position
    
    // save current position
    curPos = ftell(stream);
    
    // skip to end and get size in bytes
    fseek(stream, 0L, SEEK_END);
    totalSize = ftell(stream);
    
    // return to current position
    fseek(stream, curPos, SEEK_SET);
    
    return ((totalSize - curPos) / BytesPerSample);
}

void CloseAudio(FILE *stream) {
    pclose(stream);
}
