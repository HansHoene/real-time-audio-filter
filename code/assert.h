/*
    File: "assert.h"
    Author: Hans-Edward Hoene
    
    This header file defines a module for assertions.
*/

#ifndef _ASSERT_H_
#define _ASSERT_H_

#include <stdio.h>
#include <stdlib.h>

#define Assert(cond, msg)    { \
    if (!(cond)) { \
        printf("\n\n\a*** Assertion Failed\n"); \
        printf("  File: \"%s\"\n", __FILE__); \
        printf("  Line Number: %u\n", __LINE__); \
        printf("  Message: \"%s\"\n\n", msg); \
        exit(-1); \
    } \
}

#endif
