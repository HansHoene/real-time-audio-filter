/*
    File: "MemMgr.c"
    Author: Hans-Edward Hoene
*/

#include "MemMgr.h"

#include "Audio.h"  // sample
#include "assert.h" // Assert

#include <stdint.h>
#include <pthread.h> // mutex
#include <semaphore.h>

#define NumBfrs         (30)
#define FALSE           (0)
#define TRUE            (!FALSE)

static Buffer           buffers[NumBfrs];
static int              available[NumBfrs];
static pthread_mutex_t  lock;
static sem_t            numAvailable;

void InitMemMgr() {
    unsigned int i;
    int flag;
    
    // mark all buffers as available
    for (i = 0; i < NumBfrs; i++) {
        available[i] = TRUE;
    }
    
    // initialise mutex
    flag = pthread_mutex_init(&lock, NULL);
    Assert(flag == 0, "Could not create mutex");
    
    // initialise semaphore
    flag = sem_init(&numAvailable, 0, NumBfrs);
    Assert(flag == 0, "Could not create semaphore");
}

Buffer *AllocateBuffer() {
    unsigned int    i;
    Buffer          *ret;   // return value
    
    // find available buffer
    sem_wait(&numAvailable);
    pthread_mutex_lock(&lock);
    ret = NULL;
    for (i = 0; (i < NumBfrs) && (ret == NULL); i++) {
        // if available, mark as not available and get pointer
        if (available[i]) {
            available[i] = FALSE;
            ret = &buffers[i];
        }
    }
    pthread_mutex_unlock(&lock);
    
    Assert(ret != NULL, "No more buffers");
    return ret;
}

void FreeBuffer(Buffer *bfr) {
    unsigned int i;
    int flag;
    
    // match pointer with a buffer index
    i = bfr - buffers;
    Assert(i < NumBfrs, "Trying to free an invalid buffer");
    
    // mark buffer as available
    pthread_mutex_lock(&lock);
    flag = available[i];
    available[i] = TRUE;
    sem_post(&numAvailable);
    pthread_mutex_unlock(&lock);
    
    Assert(!flag, "Freeing a buffer that is available");
}

void DestroyMemMgr() {
    pthread_mutex_destroy(&lock);
}
