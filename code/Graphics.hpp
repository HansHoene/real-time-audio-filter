/*
    File: "Graphics.hpp"
    Author: Hans-Edward Hoene
    
    This module displays real-time graphics.
*/

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "cplx.h"
#include "MemMgr.h"

// display buffer                   
typedef struct {
    float   dataIn[ BfrSize];   // input data
    float   specIn[ BfrSize];   // input spectrum
    float   specOut[BfrSize];   // output spectrum
    float   dataOut[BfrSize];   // output data
} DisplayBfr;

/*
    Purpose: initialise real-time graphics
*/
void InitGraphics();

/*
    Purpose: Try to update spectrum graphics.
    Arguments:
        const DisplayBfr *bfr - display data
*/
void UpdateGraphics(const DisplayBfr *bfr);

/*
    Purpose: Destroy graphics
*/
void DestroyGraphics();

#endif
