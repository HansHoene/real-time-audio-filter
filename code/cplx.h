/*
	File: "cplx.h"
	Author: Hans-Edward Hoene
*/

#ifndef _CPLX_H_
#define _CPLX_H_

typedef struct {
    float real;
    float imag;
} cplx;

/*
    Purpose: multiply two complex numbers (result = arg1 * arg2)
    Arguments:
        cplx *dst - the result
        cplx *a   - arg1
        cplx *b   - arg2
    Note: The result is allowed point to the same address space as an argument.
*/
void MultiplyComplex(cplx *dst, cplx *a, cplx *b);

/*
    Purpose: add two complex numbers (result = arg1 + arg2)
    Arguments:
        cplx *dst - the result
        cplx *a   - arg1
        cplx *b   - arg2
    Note: The result is allowed point to the same address space as an argument.
*/
void AddComplex(cplx *dst, cplx *a, cplx *b);

/*
    Purpose: subtract two complex numbers (result = arg1 - arg2)
    Arguments:
        cplx *dst - the result
        cplx *a   - arg1
        cplx *b   - arg2
    Note: The result is allowed point to the same address space as an argument.
*/
void SubtractComplex(cplx *dst, cplx *a, cplx *b);

#endif
