/*
    File: "IODriver.c"
    Author: Hans-Edward Hoene
*/

#include "IODriver.h"

#include "Audio.h"
#include "MemMgr.h"
#include "assert.h"

#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define FALSE   (0)
#define TRUE    (!FALSE)

#define BackLog (10) // largest number of buffers that can be filled or emptied
                     // before a pipeline stall

/*
    Buffer Array
    
    Hold circular queue buffers.
*/
typedef struct {
    FILE            *stream;            // stream
    Buffer          *bfrs[BackLog];     // array of buffers
    sem_t           numFull;            // number of 
    sem_t           numEmpty;           // full?
    unsigned int    get;                // index to get next buffer from
    unsigned int    put;                // index to put next buffer into
} BfrArr;

static BfrArr       in;     // buffer array for microphone
static BfrArr       out;    // buffer array for speaker

static pthread_t    reader;
static pthread_t    writer;

static int          exitProgram;

/* ------------------- H e l p e r    F u n c    D e c s -------------------- */

/*
    Purpose: Run thread to read samples from microphone
*/
void *RunReader(void *not_used);

/*
    Purpose: Run thread to write samples to speaker
*/
void *RunWriter(void *not_used);

/* ----------------------- F u n c t i o n    D e f s ----------------------- */

void InitIO() {
    int flag; // error flag
    
    // open microphone input
#ifdef __linux__
    in.stream = OpenLinuxMicrophone();
#else
    in.stream = OpenMicrophone(MyMic);
    //in.stream = OpenAudioInput("..\\Data Sets\\freeloader.mp3");
#endif
    Assert(in.stream != NULL, "Could not open microphone input");
    
    // open speaker output
    out.stream = OpenSpeaker();
    Assert(out.stream != NULL, "Could not open speaker output");
    
    // initialise buffer system for input
    in.get = 0;
    in.put = 0;
    flag = sem_init(&in.numFull, 0, 0);
    Assert(flag == 0, "Could not initialise semaphore");
    flag = sem_init(&in.numEmpty, 0, BackLog);
    Assert(flag == 0, "Could not initialise semaphore");
    
    // initialise buffer system for output
    out.get = 0;
    out.put = 0;
    flag = sem_init(&out.numFull, 0, 0);
    Assert(flag == 0, "Could not initialise semaphore");
    flag = sem_init(&out.numEmpty, 0, BackLog);
    Assert(flag == 0, "Could not initialise semaphore");
    
    // start threads
    exitProgram = FALSE;
    pthread_create(&reader, NULL, RunReader, NULL);
    Assert(flag == 0, "Could not start thread");
    pthread_create(&writer, NULL, RunWriter, NULL);
    Assert(flag == 0, "Could not start thread");
}

Buffer *GetBuffer() {
    Buffer *bfr;
    
    sem_wait(&in.numFull);
    bfr = in.bfrs[in.get];
    in.get = (in.get + 1) % BackLog;
    sem_post(&in.numEmpty);
    
    return bfr;
}

void PutBuffer(Buffer *bfr) {
    sem_wait(&out.numEmpty);
    out.bfrs[out.put] = bfr;
    out.put = (out.put + 1) % BackLog;
    sem_post(&out.numFull);
}

void KillIO() {
    exitProgram = TRUE;
    
    // wait for threads to terminate
    pthread_join(reader, NULL);
    pthread_join(writer, NULL);
    
    // clear resources
    CloseAudio(in.stream);
    CloseAudio(out.stream);
    sem_destroy(&in.numFull);
    sem_destroy(&in.numEmpty);
    sem_destroy(&out.numFull);
    sem_destroy(&out.numEmpty);
}

void *RunReader(void *not_used) {
    Buffer *bfr;
    unsigned int numRead;
    
    for (;;) {
        // allocate a buffer
        bfr = AllocateBuffer();
        
        // read buffer
        numRead = ReadAudio(in.stream, bfr->data, BfrSize);
        /*if (numRead != BfrSize) {
            CloseAudio(in.stream);
            in.stream = OpenAudioInput("..\\Data Sets\\freeloader.mp3");
        }*/
        Assert(numRead == BfrSize, "Did not read full buffer size");
        
        // put buffer in circular queue
        sem_wait(&in.numEmpty);
        in.bfrs[in.put] = bfr;
        in.put = (in.put + 1) % BackLog;
        sem_post(&in.numFull);
        
        // exit now?
        if (exitProgram) {
            // put NULL in circular queue
            sem_wait(&in.numEmpty);
            in.bfrs[in.put] = NULL;
            in.put = (in.put + 1) % BackLog;
            sem_post(&in.numFull);
            
            // exit
            pthread_exit(NULL);
            return NULL;
        }
    }
    
    return NULL;
}

void *RunWriter(void *not_used) {
    Buffer *bfr;
    unsigned int numWrite;
    
    for(;;) {
        // get buffer from circular queue
        sem_wait(&out.numFull);
        bfr = out.bfrs[out.get];
        out.get = (out.get + 1) % BackLog;
        sem_post(&out.numEmpty);
        
        // exit?
        if (bfr == NULL) {
            pthread_exit(NULL);
            return NULL;
        }
        
        // write buffer
        numWrite = WriteAudio(out.stream, bfr->data, BfrSize);
        Assert(numWrite == BfrSize, "Did not write full buffer size");
        
        // free buffer
        FreeBuffer(bfr);
    }
    
    return NULL;
}
