#include "Audio.h"

#include <stdio.h>
#include <stdint.h>

#define FILE_IN     "whistle.mp3"
#define FILE_OUT    "whistle-copy.mp3"
 
#define BfrSize     (1000)
 
int main() {
    FILE                *in;            // input audio
    FILE                *out;           // output audio
    
    unsigned int        numRead;        // # of samples read into buffer
    long unsigned int   count;          // total # of samples read
    short int           buf[BfrSize];   // buffer
    
    // open input
    in = OpenAudioInput(FILE_IN);
    if (in == NULL) {
        printf("Error.\n");
        return 0;
    }
    
    // open output
    out = OpenAudioOutput(FILE_OUT);
    if (out == NULL) {
        CloseAudio(in);
        printf("Error.\n");
        return 0;
    }

    // read and write samples
    count = 0;
    while ((numRead = ReadAudio(in, buf, BfrSize)) > 0) {
        count += numRead;
        WriteAudio(out, buf, numRead);
    }
    printf("Total # of samples read = %lu\n", count);

    // close
    CloseAudio(in);
    CloseAudio(out);

    return 0;
}
