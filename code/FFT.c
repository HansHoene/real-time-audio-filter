/*
    File: "FFT.c"
    Author: Hans-Edward Hoene
*/

#include "cplx.h"

#define _USE_MATH_DEFINES 
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#define FALSE   (0)
#define TRUE    (!FALSE)

/*
    Purpose: Shift the spectrum so that the FFT is centred on zero.
    Arguments:
        [in,out] cplx          data[]  - data
        [in    ] unsigned int  N       - array size
*/
static void Shift(cplx data[], unsigned int N);

/*
    Purpose: Reverse the bits in a number
    Arguments:
        unsigned int  N    - The maximum possible number plus one.
                           - # bits = log_2(N) + 1.
                           - e.g., if N is 8, then only 3 significant bits.
                           - must be a power of 2.
        unsigned int  num  - the number with bits to reverse
    Return:
        The result
*/
static unsigned int ReverseBits(unsigned int N, unsigned int num);

/*
    Purpose: Run a 1D FFT on a section of the image
    Note: Instead of a 1D DFT, which I had before, I implemented an FFT, which
    is much faster.  I learnt about the algorithm from ...
    "https://engineering.purdue.edu/~ipollak/ee438/FALL04/notes/Section1.4.pdf".
    Arguments:
        cplx          data[]   - image data; start of array
        unsigned int  N        - number of discrete samples in row or column
        int           forward  - boolean: FFT? or inverse FFT?
*/
static void FFT(cplx data[], unsigned int N, int forward);

/* -------------------- P u b l i c    F u n c    D e f --------------------- */

int DFT(cplx data[], unsigned int size, int forward) {
    // error check: is N a power of 2?
    if ((size & (size - 1)) != 0) {
        return FALSE;
    }
    
    // shift spectrum so that frequency zero is in centre of array
    if (forward) {
        Shift(data, size);
    }
    
    // run FFT
    FFT(data, size, forward);
    
    // on inverse FFT, undo the shift
    // this works because odd numbers are multiplied by -1 again
    if (!forward) {
        Shift(data, size);
    }
    
    return TRUE;
}

/* ------------------- H e l p e r    F u n c    D e f s -------------------- */

static void Shift(cplx data[], unsigned int N) {
    unsigned int i;
    
    // multiply all odd samples by -1
    for (i = 1; i < N; i += 2) {
        data[i].real *= -1;
        data[i].imag *= -1;
    }
}

static unsigned int ReverseBits(unsigned int N, unsigned int num) {
    unsigned int result;
    
    result = 0;
    for (N -= 1; N != 0; N >>= 1) {
        result <<= 1;
        result |= (num & 0x1);
        num >>= 1;
    }
    
    return result;
}

static void FFT(cplx data[], unsigned int N, int forward) {
    unsigned int    i;
    unsigned int    k;      // index for FT
    unsigned int    skip;   // for FFT, skip = N/2
    unsigned int    size;   // size of input array
    
    cplx            WN;     // WN = exp(+/- 2 * pi / N)
    cplx            mult;   // mult is pow(WN, k)
    cplx            temp;   // used for swapping
    
    // set size equal to N
    // N will be used as a variable to record size for recursive FFTs
    size = N;
    
    // re-order inputs.
    // every index is swapped with the bit reversal of that index.
    // e.g., if N is 8 (3 bits), then index 6 (110) -> index 3 (011).
    // This allows FFT to happen iteratively as a Breadth-First Search (BFS)
    // rather than recursively.
    for (i = 0; i < size; i++) {
        k = ReverseBits(size, i);
        if (i < k) {
            // swap data[i] with data[j]
            temp = data[i];
            data[i] = data[k];
            data[k] = temp;
        }
    }
    
    // run DFT algorithm
    for (N = 2; N <= size; N <<= 1) {
        // get W_N = exp(-2pi / N) (or exp(2pi / N) for backward)
        WN.real = cos(2.0 * M_PI / N);
        WN.imag = sin(2.0 * M_PI / N);
        if (forward) {
            WN.imag *= -1;
        }
        
        // set skip is half of N
        skip = N >> 1;
        
        // set mult is pow(W_N, 0)
        mult.real = 1;
        mult.imag = 0;
        
        // loop through first half of values in Fourier Transform
        for (k = 0; k < skip; k++) {
            
            // repeat for every single group of FTs in the array
            // each FT group is of size N
            for (i = k; i < size; i += N) {
                // multiply second half of the array by pow(W_N, k)
                MultiplyComplex(&data[i + skip], &data[i + skip], &mult);
                
                // set X(k)       = X(k) + pow(W_N, k) X(k + N/2)
                // set X(k + N/2) = X(k) - pow(W_N, k) X(k + N/2)
                temp = data[i];
                AddComplex(     &data[i       ], &temp, &data[i + skip]);
                SubtractComplex(&data[i + skip], &temp, &data[i + skip]);
            }
            
            // update multiplier so that mult = pow(W_N, k)
            MultiplyComplex(&mult, &mult, &WN);
        }
    }
    
    // normalise values for forward FFT
    if (forward) {
        for (i = 0; i < size; i++) {
            data[i].real /= size;
            data[i].imag /= size;
        }
    }
}
