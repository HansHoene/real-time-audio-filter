/*
    File: "Processor.h"
    Author: Hans-Edward Hoene
    
    This module processes buffers.
*/

#ifndef _PROCESSOR_H_
#define _PROCESSOR_H_

/*
    Purpose: Start the processor
*/
void StartProcessor();

/*
    Purpose: Wait for processor to terminate
*/
void StopProcessor();

#endif
