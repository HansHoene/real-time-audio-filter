/*
    File: "Audio.h"
    Author: Hans-Edward Hoene
    
    This file defines functions for opening audio streams.  These functions
    rely on ffmpeg.  FFMPEG is a "complete, cross-platform solution to record,
    convert and stream audio and video".  Read more at <<https://ffmpeg.org/>>.
    
    I learnt of this method from Ted Burke at the follwing link:
    <<https://batchloaf.wordpress.com/2017/02/10/a-simple-way-to-read-and-write-audio-and-video-files-in-c-using-ffmpeg/>>.
    
    The audio driver automatically uses the following parameters.
    Sampling Frequency = 44100 Hz
    Bit depth = 16 (signed)
    Channels = mono
*/

#ifndef _AUDIO_H_
#define _AUDIO_H_

#include <stdio.h>  // size_t, FILE
#include <stdint.h> // sint16_t

// samples are signed 16-bit integers
typedef int16_t sample;

// my default microphone and speaker
#define MyMic     ("Microphone (Realtek High Definition Audio)")
#define MySpeaker ("Stereo Mix (Realtek High Definition Audio)")

/*
    Purpose: Open an audio input stream
    Arguments:
        [in ] const char fname[] - file name
    Return:
        A file handle, or NULL on an error
*/
FILE *OpenAudioInput(const char fname[]);

/*
    Purpose: Read samples from an audio stream
    Arguments:
        [in ] FILE          *stream - the audio stream
        [out] sample        bfr[]   - buffer to read samples into
        [in ] unsigned int  bfrSize - # of samples to read
    Return:
        The # of samples read.
*/
size_t ReadAudio(FILE *stream, sample bfr[], unsigned int bfrSize);

/*
    Purpose: Open an audio output stream
    Arguments:
        [in ] const char fname[] - file name
    Return:
        A file handle, or NULL on an error
*/
FILE *OpenAudioOutput(const char fname[]);

/*
    Purpose: Write samples to an audio stream
    Arguments:
        [in ] FILE          *stream - the audio stream
        [out] sample        bfr[]   - buffer to write samples from
        [in ] unsigned int  bfrSize - # of samples to write
    Return:
        The # of samples written.
*/
size_t WriteAudio(FILE *stream, sample bfr[], unsigned int bfrSize);

/*
    Purpose: Open audio input from microphone
    Note: This is OS-specific for Windows DSHOW devices.
    Arguments:
        [in ] const char device[] - device name
    Return:
        A file handle, or NULL on an error.
*/
FILE *OpenMicrophone(const char device[]);

/*
    Purpose: Open microphone in Linux machine
    Return:
        A file handle, or NULL on an error.
*/
FILE *OpenLinuxMicrophone();

/*
    Purpose: Open audio output to default speaker device
    Return:
        A file handle, or NULL on an error.
*/
FILE *OpenSpeaker();

/*
    Purpose: Get the number of samples that are buffered (not read) in input
             stream
    Arguments:
        [in ] FILE *stream - input stream
    Return:
        # of samples that are buffered
*/
long int CountBufferedData(FILE *stream);

/*
    Purpose: Close audio stream
    Arguments:
        [in ] FILE *stream
*/
void CloseAudio(FILE *stream);

#endif
