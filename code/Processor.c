/*
    File: "Processor.c"
    Author: Hans-Edward Hoene
*/

#include "Processor.h"

#include "IODriver.h"
#include "cplx.h"
#include "FFT.h"
#include "assert.h"
#include "Graphics.hpp"

#include <pthread.h>
#include <semaphore.h>
#include <math.h>

#define FALSE           (0)
#define TRUE            (!FALSE)

#define alpha   (0.0)

// filter bandwidth
#ifndef D0
#define D0 (400)
#endif

// filter order
#ifndef Order
#define Order (1)
#endif

// FilterType = 0 for low-pass filter or 1 for high-pass filter
#ifndef FilterType
#define FilterType (0)
#endif

#define DIFF(a, b)  (((a) > (b)) ? ((a) - (b)): ((b) - (a)))
#define Squared(x)  ((x) * (x))

static pthread_t proc;
static cplx complex[BfrSize];

void *RunProcessor(void *not_used);

// convert samples to complex values
void SamplesToCplx(short int samples[], cplx complex[], unsigned int size);

// convert complex values into samples
void CplxToSamples(cplx complex[], short int samples[], unsigned int size);

// apply a filter
void ApplyFilter(cplx data[], unsigned int size);

void StartProcessor() {
    int flag;
    
    // start processor
    flag = pthread_create(&proc, NULL, RunProcessor, NULL);
    Assert(flag == 0, "Could not start thread");
}

void StopProcessor() {
    pthread_join(proc, NULL);
}

void *RunProcessor(void *not_used) {
    Buffer *bfr;
    DisplayBfr dispBfr;
    unsigned int i;
    
    for (;;) {
        // get input buffer
        bfr = GetBuffer();
        
        // exit?
        if (bfr == NULL) {
            PutBuffer(NULL);
            pthread_exit(NULL);
            return NULL;
        }
        
        // copy samples to display buffer
        for (i = 0; i < BfrSize; i++) {
            dispBfr.dataIn[i] = (alpha * dispBfr.dataIn[i]) + ((1 - alpha) * bfr->data[i]);
        }
        
        // copy buffer contents into array of complex values
        SamplesToCplx(bfr->data, complex, BfrSize);
        
        // convert time domain samples to frequency domain
        DFT(complex, BfrSize, TRUE);
        
        // copy spectral values to display buffer
        //printf("[");
        for (i = 0; i < BfrSize; i++) {
            dispBfr.specIn[i] = (alpha * dispBfr.specIn[i]) + ((1 - alpha) * sqrtf(
                Squared(complex[i].real) + Squared(complex[i].imag)
            ));
            //printf("%f, ", dispBfr.specIn[i]);
        }
        //printf("]\n");
        
        // filter
        ApplyFilter(complex, BfrSize);
        
        // copy output spectral values to display buffer
        for (i = 0; i < BfrSize; i++) {
            dispBfr.specOut[i] = (alpha * dispBfr.specOut[i]) + ((1 - alpha) *  sqrtf(
                Squared(complex[i].real) + Squared(complex[i].imag)
            ));
        }
        
        // convert frequencies back to time domain
        DFT(complex, BfrSize, FALSE);
        
        // convert complex values to samples
        CplxToSamples(complex, bfr->data, BfrSize);
        
        // copy output samples to display buffer
        for (i = 0; i < BfrSize; i++) {
            dispBfr.dataOut[i] = (alpha * dispBfr.dataOut[i]) + ((1 - alpha) * bfr->data[i]);
        }
        
        // try to update graphics
        UpdateGraphics(&dispBfr);
        
        // write result to output stream
        PutBuffer(bfr);
    }
    
    return NULL;
}

void SamplesToCplx(short int samples[], cplx complex[], unsigned int size) {
    unsigned int i;
    
    for (i = 0; i < size; i++) {
        complex[i].real = (float) samples[i];
        complex[i].imag = (float) 0;
    }
}

void CplxToSamples(cplx complex[], short int samples[], unsigned int size) {
    unsigned int    i;
    
    for (i = 0; i < size; i++) {
        samples[i] = (short int) roundf( sqrtf(
            (complex[i].real * complex[i].real) +
            (complex[i].imag * complex[i].imag)
        ));
        if (complex[i].real < 0) {
            samples[i] *= -1;
        }
    }
}

void ApplyFilter(cplx data[], unsigned int size) {
    unsigned int i;
    unsigned int centre;
    unsigned int dist;
    float invH;
    
    centre = (size >> 1);
    for (i = 0; i < size; i++) {
        dist = DIFF(i, centre);
        
#if FilterType == 0
        // butterworth low-pass
        invH = (dist * 44100.0 / size) / D0;
        invH = 1 + pow(invH, Order * 2);
        data[i].real /= invH;
        data[i].imag /= invH;
#elif FilterType == 1
        // butterworth high-pass
        invH = D0 / (dist * 44100.0 / size);
        invH = 1 + pow(invH, Order * 2);
        data[i].real /= invH;
        data[i].imag /= invH;
#elif FilterType == 2
        // low-pass
        if ((dist * 44100.0 / size) > D0) {
            data[i].real = 0;
            data[i].imag = 0;
        }
#elif FilterType == 3
        // high-pass
        if ((dist * 44100.0 / size) < D0) {
            data[i].real = 0;
            data[i].imag = 0;
        }
#else
        // nothing
#endif
        
    }
}
