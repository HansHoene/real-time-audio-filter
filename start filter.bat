SET "PATH=%PATH%;C:\Users\hansh\Documents\GitHub\real-time-audio-filter\ffmpeg-4.1.1-win64-static\bin"

SET /p "vars=Enter Makefile Parameters: "

cd code
make clean
del *.exe
make %vars%
make clean

if exist realtime.exe (
    realtime.exe
)

cd ..
pause
exit /b 0
